const express = require('express');

const User = require('../models/user');

function create(req, res, next) {
    let email = req.body.email;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let password = req.body.password;

    let user = new User({
        email: email,
        name: name,
        lastName: lastName,
        password: password
    });

    user.save().then(obj => res.status(200).json({
        message: 'Usuario creado correctamente',
        objs: obj
    })).catch(err => res.status(500).json({
        message: 'No se pudo almacenar el usuario',
        objs: err
    }));

}

function list(req, res, next){
    let page = req.params.page ? req.params.page : 0;
    User.find().then(objs => res.status(200).json({
        message: "Usuarios del sistema",
        objs: objs
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar los usuarios del sistema",
        objs: err
    }));
}

function index(req, res, next){
    const id = req.params.id;
    User.findOne({"_id": id}).then(obj => res.status(200).json({
        message: `Usuario del sistema con id: ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar los usuarios del sistema",
        objs: err
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    let email = req.body.email;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let password = req.body.password;
    
    let user = new Object();
    
    if(email){
        user._email = email;
    }

    if(name){
        user._name = name;
    }

    if(lastName){
        user._lastName = lastName;
    }

    if(password){
        user._password = password;
    }

    User.findOneAndUpdate({"_id":id}, user).then(obj => res.status(200).json({
        message: `Usuario del sistema con id: ${id} se ha modificado`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron modificar los atributos del usuario del sistema",
        objs: err
    }));

}

function replace(req, res, next){
    const id = req.params.id;
    let email = req.body.email ? req.body.email : "";
    let name = req.body.name ? req.body.name : "";
    let lastName = req.body.lastName ? req.body.lastName : "";
    let password = req.body.password ? req.body.password : "";

    let user = new Object({
        _email: email,
        _name: name,
        _lastName: lastName,
        _password: password
    });

    User.findOneAndReplace({"_id": id}, user).then(obj => res.status(200).json({
        mesage: `Se reemplazo el usuario del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede reemplazar el usuario",
        objs: err
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    User.remove({"_id":id}).then(obj => res.status(200).json({
        mesage: `Se elimino el usuario del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede eliminar el usuario",
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}