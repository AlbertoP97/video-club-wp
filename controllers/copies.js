const express = require('express');

const Copy = require('../models/copy');

function create(req, res, next) {
    let format = req.body.format;
    let movie = req.body.movie;
    let number = req.body.number;
    let status = req.body.status;

    let copy = new Copy({
        format: format,
        movie: movie,
        number: number,
        status: status
    });

    copy.save().then(obj => res.status(200).json({
        message: 'Copia generada correctamente',
        objs: obj
    })).catch(err => res.status(500).json({
        message: 'No se pudo almacenar la copia',
        objs: err
    }));
}

function list(req, res, next){
    let page = req.params.page ? req.params.page : 0;
    Copy.find().then(objs => res.status(200).json({
        message: "Copias del sistema",
        objs: objs
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar las copias de las pelicuas",
        objs: err
    }));
}

function index(req, res, next){
    const id = req.params.id;
    Copy.findOne({"_id": id}).then(obj => res.status(200).json({
        message: `Copia de la pelicula con id: ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar las copias de la pelicula",
        objs: err
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    let format = req.body.format;
    let movie = req.body.movie;
    let number = req.body.number;
    let status = req.body.status;
    
    let copy = new Object();
    
    if(format){
        copy._format = format;
    }

    if(movie){
        copy._moive = movie;
    }

    if(number){
        copy._number = number;
    }

    if(status){
        copy._status = status;
    }

    Copy.findOneAndUpdate({"_id":id}, copy).then(obj => res.status(200).json({
        message: `Copia de la pelicula con id: ${id} se ha modificado`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron modificar los atributos de las copias de las peliculas",
        objs: err
    }));

}

function replace(req, res, next){
    const id = req.params.id;
    let format = req.body.format ? req.body.format : "";
    let movie = req.body.movie ? req.body.movie : "";
    let number = req.body.number ? req.body.number : "";
    let status = req.body.status ? req.body.status : "";

    let copy = new Object({
        _format: format,
        _movie: movie,
        _number: number,
        _status: status
    });

    Copy.findOneAndReplace({"_id": id}, copy).then(obj => res.status(200).json({
        mesage: `Se reemplazo la copia de la pelicula con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede reemplazar la copia",
        objs: err
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    Copy.remove({"_id":id}).then(obj => res.status(200).json({
        mesage: `Se elimino la copia del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede eliminar la copia",
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}