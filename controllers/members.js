const express = require('express');

const Member = require('../models/member');

function create(req, res, next) {
    let address = req.body.address;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let phone = req.body.phone;
    let status = req.body.status;
    let addressCity = req.body.addressCity;
    let addressCountry = req.body.addressCountry;
    let addressNumber = req.body.addressNumber;
    let addressState = req.body.addressState;
    let addressStreet = req.body.addressStreet;


    let member = new Member({
        address: address,
        name: name,
        lastName: lastName,
        phone: phone,
        status: status,
        addressCity: addressCity,
        addressCountry: addressCountry,
        addressNumber: addressNumber,
        addressState: addressState,
        addressStreet: addressStreet,
    });

    member.save().then(obj => res.status(200).json({
        message: 'Miembro creado correctamente',
        objs: obj
    })).catch(err => res.status(500).json({
        message: 'No se pudo almacenar el miembro',
        objs: err
    }));

}

function list(req, res, next){
    let page = req.params.page ? req.params.page : 0;
    Member.find().then(objs => res.status(200).json({
        message: "Miembros del sistema",
        objs: objs
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar los miembros del sistema",
        objs: err
    }));
}

function index(req, res, next){
    const id = req.params.id;
    Member.findOne({"_id": id}).then(obj => res.status(200).json({
        message: `Miembro del sistema con id: ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar los miembros del sistema",
        objs: err
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    let address = req.body.address;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let phone = req.body.phone;
    let status = req.body.status;
    let addressCity = req.body.addressCity;
    let addressCountry = req.body.addressCountry;
    let addressNumber = req.body.addressNumber;
    let addressState = req.body.addressState;
    let addressStreet = req.body.addressStreet;
    
    let member = new Object();
    
    if(address){
        member._address = address;
    }

    if(name){
        member._name = name;
    }

    if(lastName){
        member._lastName = lastName;
    }

    if(phone){
        member._phone = phone;
    }

    if(status){
        member._status = status;
    }

    if(addressCity){
        member._addressCity = addressCity;
    }

    if(addressCountry){
        member._addressCountry = addressCountry;
    }

    if(addressNumber){
        member._addressNumber = addressNumber;
    }

    if(addressState){
        member._addressState = addressState;
    }

    if(addressStreet){
        member._addressStreet = addressStreet;
    }

    Member.findOneAndUpdate({"_id":id}, member).then(obj => res.status(200).json({
        message: `Miembro del sistema con id: ${id} se ha modificado`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron modificar los atributos del miembro del sistema",
        objs: err
    }));

}

function replace(req, res, next){
    const id = req.params.id;
    let address = req.body.address ? req.body.address : "";
    let name = req.body.name ? req.body.name : "";
    let lastName = req.body.lastName ? req.body.lastName : "";
    let phone = req.body.phone ? req.body.phone : "";
    let status = req.body.status ? req.body.status : "";
    let addressCity = req.body.addressCity ? req.body.addressCity : "";
    let addressCountry = req.body.addressCountry ? req.body.addressCountry : "";
    let addressNumber = req.body.addressNumber ? req.body.addressNumber : "";
    let addressState = req.body.addressState ? req.body.addressState : "";
    let addressStreet = req.body.addressStreet ? req.body.addressStreet : "";

    let member = new Object({
        _address: address,
        _name: name,
        _lastName: lastName,
        _phone: phone,
        _status: status,
        _addressCity: addressCity,
        _addressCountry: addressCountry,
        _addressNumber: addressNumber,
        _addressState: addressState,
        _addressStreet: addressStreet,
    });

    Member.findOneAndReplace({"_id": id}, member).then(obj => res.status(200).json({
        mesage: `Se reemplazo el miembro del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede reemplazar el miembro",
        objs: err
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    Member.remove({"_id":id}).then(obj => res.status(200).json({
        mesage: `Se elimino el miembro del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede eliminar el miembro",
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}