const express = require('express');

const Movie = require('../models/movie');

function create(req, res, next) {
    let genre = req.body.genre;
    let title = req.body.title;
    let director = req.body.director;
    let actors = req.body.actors;
    let director = req.body.director;
    let actors = req.body.actors;
    let directorName = req.body.directorName;
    let directorLastName = req.body.directorLastName;
    let actorsName = req.body.actorsName;
    let actorsLastName = req.body.actorsLastName;

    let movie = new Movie({
        genre: genre,
        title: title,
        director: director,
        actors: actors,
        directorName: directorName,
        directorLastName: directorLastName,
        actorsName: actorsName,
        actorsLastName: actorsLastName
    });

    movie.save().then(obj => res.status(200).json({
        message: 'Pelicula creada correctamente',
        objs: obj
    })).catch(err => res.status(500).json({
        message: 'No se pudo almacenar la pelicula',
        objs: err
    }));

}

function list(req, res, next){
    let page = req.params.page ? req.params.page : 0;
    Movie.find().then(objs => res.status(200).json({
        message: "Peliculas del sistema",
        objs: objs
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar las peliculas del sistema",
        objs: err
    }));
}

function index(req, res, next){
    const id = req.params.id;
    Movie.findOne({"_id": id}).then(obj => res.status(200).json({
        message: `Pelicula del sistema con id: ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar las peliculas del sistema",
        objs: err
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    let genre = req.body.genre;
    let title = req.body.title;
    let director = req.body.director;
    let actors = req.body.actors;
    let directorName = req.body.directorName;
    let directorLastName = req.body.directorLastName;
    let actorsName = req.body.actorsName;
    let actorsLastName = req.body.actorsLastName;
    
    let movie = new Object();
    
    if(genre){
        movie._genre = genre;
    }

    if(title){
        movie._title = title;
    }

    if(director){
        movie._director = director;
    }

    if(actors){
        movie._actors = actors;
    }

    if(directorName){
        movie._directorName = directorName;
    }

    if(directorLastName){
        movie._directorLastName = directorLastName;
    }

    if(actorsName){
        movie._actorsName = actorsName;
    }

    if(actorsLastName){
        movie._actorsLastName = actorsLastName;
    }

    Movie.findOneAndUpdate({"_id":id}, movie).then(obj => res.status(200).json({
        message: `Pelicula del sistema con id: ${id} se ha modificado`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron modificar los atributos de la pelicula del sistema",
        objs: err
    }));

}

function replace(req, res, next){
    const id = req.params.id;
    let genre = req.body.genre ? req.body.genre : "";
    let title = req.body.title ? req.body.title : "";
    let director = req.body.director ? req.body.director : "";
    let actors = req.body.actors ? req.body.actors : "";
    let directorName = req.body.directorName ? req.body.directorName : "";
    let directorLastName = req.body.directorLastName ? req.body.directorLastName : "";
    let actorsName = req.body.actorsName ? req.body.actorsName : "";
    let actorsLastName = req.body.actorsLastName ? req.body.actorsLastName : "";

    let movie = new Object({
        _genre: genre,
        _title: title,
        _director: director,
        _actors: actors,
        _directorName: directorName,
        _directorLastName: directorLastName,
        _actorsName: actorsName,
        _actorsLastName: actorsLastName
    });

    Movie.findOneAndReplace({"_id": id}, movie).then(obj => res.status(200).json({
        mesage: `Se reemplazo la pelicula del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede reemplazar la pelicula",
        objs: err
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    Movie.remove({"_id":id}).then(obj => res.status(200).json({
        mesage: `Se elimino la pelicula del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede eliminar la pelicula",
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}