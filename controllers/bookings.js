const express = require('express');

const Booking = require('../models/booking');

function create(req, res, next) {
    let copy = req.body.copy;
    let member = req.body.member;
    let date = req.body.date;

    let booking = new Booking({
        copy: copy,
        member: member,
        date: date
    });

    booking.save().then(obj => res.status(200).json({
        message: 'Renta generada correctamente',
        objs: obj
    })).catch(err => res.status(500).json({
        message: 'No se pudo generar la renta',
        objs: err
    }));

}

function list(req, res, next){
    let page = req.params.page ? req.params.page : 0;
    Booking.find().then(objs => res.status(200).json({
        message: "Rentas de la pelicula",
        objs: objs
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar las rentas de la pelicula",
        objs: err
    }));
}

function index(req, res, next){
    const id = req.params.id;
    Booking.findOne({"_id": id}).then(obj => res.status(200).json({
        message: `Renta con id: ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron encontrar las rentas de la pelicula",
        objs: err
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    let copy = req.body.copy;
    let member = req.body.member;
    let date = req.body.date;

    let booking = new Object();
    
    if(copy){
        booking._copy = copy;
    }

    if(member){
        booking._member = member;
    }

    if(date){
        booking._date = date;
    }

    Booking.findOneAndUpdate({"_id":id}, booking).then(obj => res.status(200).json({
        message: `Renta de la pelicula con id: ${id} se ha modificado`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se pudieron modificar los atributos de la renta del sistema",
        objs: err
    }));

}

function replace(req, res, next){
    const id = req.params.id;
    let copy = req.body.copy;
    let member = req.body.member;
    let date = req.body.date;


    let booking = new Object({
        _copy: copy,
        _member: member,
        _date: date
    });

    Booking.findOneAndReplace({"_id": id}, booking).then(obj => res.status(200).json({
        mesage: `Se reemplazo la renta del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede reemplazar la renta",
        objs: err
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    Booking.remove({"_id":id}).then(obj => res.status(200).json({
        mesage: `Se elimino la renta del sistema con id ${id}`,
        objs: obj
    })).catch(err => res.status(500).json({
        message: "No se puede eliminar la renta",
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}