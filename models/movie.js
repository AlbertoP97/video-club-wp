const mongoose = require('mongoose');
const { list } = require('../controllers/users');

const schema = mongoose.Schema({
    _genre: String,
    _title: String,
    _director: Map,
    _actors: list,
    _directorName : String,
    _directorLastName : String,
    _actorsName : String,
    _actorsLastName : String
});

class Movie {

    constructor(genre, title, director, actors, directorName, directorLastName, actorsName, actorsLastName){
        this._genre = genre;
        this._title = title;
        this._director = director;
        this._actors = actors;
        this._directorName = directorName;
        this._directorLastName = directorLastName;
        this._actorsName = actorsName;
        this._actorsLastName = actorsLastName;
    }

    get genre(){
        return this._genre;
    }

    set genre(v){
        this._genre = v;
    }

    get title(){
        return this._title;
    }

    set title(v){
        this._title = v;
    }

    get director(){
        return this._director;
    }

    set director(v){
        this._director = v;
    }

    get actors(){
        return this._actors;
    }

    set actors(v){
        this._actors = v;
    }

    get directorName(){
        return this._directorName;
    }

    set directorName(v){
        this._directorName = v;
    }

    get directorLastName(){
        return this._directorLastName;
    }

    set directorLastName(v){
        this._directorLastName = v;
    }

    get actorsName(){
        return this._actorsName;
    }

    set actorsName(v){
        this._actorsName = v;
    }

    get actorsLastName(){
        return this._actorsLastName;
    }

    set actorsLastName(v){
        this._actorsLastName = v;
    }

}

schema.loadClass(Movie);
module.exports = mongoose.model('Movie', schema);